'use strict';

const restify          = require( 'restify' );
const feedHandler      = require( './handlers/feed' );
const graphHandler     = require( './handlers/graph' );
const loginHandler     = require( './handlers/login' );
const addUserHandler   = require( './handlers/addUser' );
const listUsersHandler = require( './handlers/listUsers' );
const addFriendHandler = require( './handlers/addFriend' );
//start and cache mongoose
const mongoose         = require( './helpers/mongoose' );

const server = restify.createServer();

server.use( restify.bodyParser() );
server.post( '/feed', feedHandler );
server.post( '/search', graphHandler );
server.post( '/login', loginHandler );
server.post( '/user/add', addUserHandler );
server.post( '/friends/list', listUsersHandler );
server.post( '/friends/add', addFriendHandler );

server.listen(5555, function() {
    console.log( 'Server listening on port 5555' );
});
