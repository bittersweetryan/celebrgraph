'use strict';

const _        = require( 'lodash' );
const UserModel = require( '../helpers/mongoose' ).model.User;

function addFriendHandler( req, res, next ){

    const email  = _
        .chain( req.body )
        .get( 'userEmail', '' )
        .escape()
        .value();

    const friendEmail  = _
        .chain( req.body )
        .get( 'friendEmail', '' )
        .escape()
        .value();

    UserModel.findOne( { email }, ( err, user ) => {
        if( err || !user ){
            res.status( 500 );
            res.send( { error: true } );
            next();
        }
        else{
            UserModel.findOne( { email : friendEmail }, ( err, friend ) => {
                user.friends.push( friendEmail );
                user.feed.unshift( `You became friends with ${friend.firstName} ${friend.lastName}!` );

                friend.friends.push( email );
                friend.feed.unshift( `You became friends with ${user.firstName} ${user.lastName}!` )
                friend.save( ( err, newFriend ) => {
                    user.save( ( err, newUser ) => {
                        if( err ){
                            res.status( 500 );
                            res.send( { success: false } );
                        }
                        else{
                            res.send( { success: true } );
                        }
                        next();
                    } );
                } );
            });
        }
    } );
}

module.exports = addFriendHandler;
