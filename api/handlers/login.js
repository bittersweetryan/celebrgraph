'use strict';

const _         = require( 'lodash' );
const mongoose  = require( '../helpers/mongoose' ).mongoose;
const UserModel = require( '../helpers/mongoose' ).model.User;

function loginHandler( req, res, next ){
    const ref = new Firebase("https://incandescent-torch-2234.firebaseio.com/");
    const email = _.escape( req.params.email );
    const password = _.escape( req.params.password );

    ref.authWithPassword( {
        email,
        password
    }, ( error, authData ) => {
        let users;

        if( error ){
            res.status( 500 );
            res.send( {
                success : false
            } );
            next();
        }
        else{
            UserModel.findOne( { email }, ( err, user ) => {

                if( err || !user ){
                    res.status( 500 );
                    res.send( { success : false } );
                }
                else{
                    res.send( {
                        success: true,
                        user : {
                            id        : user._id.id,
                            firstName : user.firstName,
                            lastName  : user.lastName,
                            email     : user.email
                        }
                    } );
                }

                next();
            } );
        }
    } );
}

module.exports = loginHandler;
