'use strict';

const Firebase  = require( 'firebase' );
const _         = require( 'lodash' );
const config    = require( '../config/config.json' ).mongo;
const mongoose  = require( '../helpers/mongoose' ).mongoose;
const UserModel = require( '../helpers/mongoose' ).model.User;

function addUserHandler( req, res, next ){
    const ref       = new Firebase("https://incandescent-torch-2234.firebaseio.com/user");
    const email     = _.chain( req.params ).get( 'email', '' ).escape().value();
    const password  = _.chain( req.params ).get( 'password', '' ).escape().value();
    const firstName = _.chain( req.params ).get( 'firstName', '' ).escape().value();
    const lastName  = _.chain( req.params ).get( 'lastName', '' ).escape().value();

    ref.createUser( {
        email,
        password
    }, ( err, userData ) => {
        if( err ){
            res.status( 500 );
            res.send( {
                success : false
            } );
            console.error( err );
            next();
        }
        else{
            //auth user created save them in the DB
            const user = new UserModel(  {
                uid : userData.uid,
                firstName,
                email,
                lastName,
                friends: [],
                feed: []
            } );

            user.save( ( error, newUser ) => {
                let success;

                if( error ){
                    res.status( 500 );
                    res.send( { success: false } )
                }
                else{
                    res.send( { success : true, user : { email, firstName, lastName } } );
                }

                next();

            } );
        }
    } );
}

module.exports = addUserHandler;
