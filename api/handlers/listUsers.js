'use strict';

const _         = require( 'lodash' );
const mongoose  = require( '../helpers/mongoose' ).mongoose;
const UserModel = require( '../helpers/mongoose' ).model.User;

function listUsers( req, res, next ){
    const email = _.get( req, 'body.email', '' );
    let userList = [];

    UserModel.findOne( { email }, ( err, user ) => {

        if( err || !user ){
            res.status( 500 );
            res.send( { error: true } );
            next();
        }
        else{
            UserModel.find( ( err, users ) => {
                const friends = _.get( user, 'friends', [] );

                userList = users.reduce( ( acc, curr ) => {

                    if( curr.email !== email && friends.indexOf( curr.email ) === -1 ){
                        acc.push( {
                            firstName : curr.firstName,
                            lastName  : curr.lastName,
                            email     : curr.email
                        } );
                    }

                    return acc;
                }, [] );

                res.send( { users :  userList } );
                next();
            } );
        }
    } );
}

module.exports = listUsers;
