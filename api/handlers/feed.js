'use strict';

const UserModel  = require( '../helpers/mongoose' ).model.User;
const _         = require( 'lodash' );

function feedHandler( req, res, next ){

    const email = _.get( req, 'body.email', '' );

    UserModel.findOne( { email }, ( err, user ) => {

        if( err || !user ){
            res.status( 500 );
            res.send( { error: true } );
        }
        else{
            res.send( { feed : user.feed || [] } );
        }
        next();
    } );
}

module.exports = feedHandler;
