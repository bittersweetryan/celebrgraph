'use strict';

const UserModel = require( '../helpers/mongoose' ).model.User;
const _         = require( 'lodash' );
const path      = require( 'dijkstrajs' ).find_path;
const async     = require( 'async' );


//mongod --config /usr/local/etc/mongod.conf
//http://jsbin.com/fewuwo/2/edit?js,console
function makeGraph( friends ){
    return _.reduce( friends, ( acc, curr ) => {
        acc[ curr ] = 1;
        return acc;
    }, {} );
}

function graphHandler( req, res, next ){
    const searchText = _.get( req, 'body.searchTerm', '' );
    const searchTerm = _.split( searchText,  ' ' );
    const email      = _.get( req, 'body.email', '' );
    let   graph      = {};
    let   result     = {};
    let   pathResult = [];


    //first make sure that the celeb exists, we need to get their email
    UserModel.findOne( { firstName : searchTerm[ 0 ] || '', lastName : searchTerm[ 1 ] || '' }, ( err, celeb ) => {

        if( err || !celeb ){
            res.send( { searchTerm : searchText, results : [], error : 'That celebrity was not found in our database yet!' } );
            return next();
        }
        else{
            UserModel.find( ( err, users ) => {

                let me;

                _.forEach( users, ( user ) => {
                    //save the current user
                    if( user.email === email ){
                        me = user;
                    }
                    graph[ user.email ] = makeGraph( user.friends || [] );
                } );

                //path throws!
                try{
                    //returns an array of email addresses
                    pathResult = path( graph, email, celeb.email );
                    pathResult.shift();

                    //get the actual usernames of the path
                    pathResult = _.map( pathResult, ( email ) => {
                        const match = _.find( users, ( user ) => {
                          return user.email === email;
                        } );

                        return `${match.firstName} ${match.lastName}`
                    } );

                    result.searchTerm = searchText;
                    result.results = pathResult;

                    me.feed.unshift( `You found you are ${pathResult.length} connections away from ${searchText}` );

                    let saves = [];
                    //add a message to your friends feeds
                    me.friends.forEach( ( friend ) => {
                        friend = _.find( users, ( curr ) => {
                            return curr.email === friend;
                        } );

                        friend.feed.unshift( `Your friend ${me.firstName} ${me.lastName} found a connection to ${searchText}.` );
                        saves.push( friend.save.bind( friend ) );

                    } );

                    async.parallel( saves, ( err, results ) =>{
                        me.save( ( err, success ) => {
                            res.send( result );
                            next();
                        } )
                    } );
                }
                catch( ex ) {
                    res.send( result );
                    next();
                }
            } );
        }
    } );


}

module.exports = graphHandler;
