'use strict'
const _ = require( 'lodash' );

function getUser( firebaseUser ){
    let val = firebaseUser.val();
    let key = _
        .chain( val )
        .keys()
        .take(1)
        .first()
        .value();

    let ret = val[ key ];
    ret.key = key;
    return ret;
}

module.exports = getUser
