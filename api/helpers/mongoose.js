
const config   = require( '../config/config.json' ).mongo;
const mongoose = require( 'mongoose' );
const schema    = require( '../config/schemas.json' );

mongoose.connect( `mongodb://localhost/${config.dbname}` );

const db = mongoose.connection;

const userSchema = mongoose.Schema( schema.User );

db.on( 'error', console.error.bind( console, 'connection error' ) );
db.once( 'open', () => {
    console.log( 'mongo db open' );
} )

process.on('SIGINT', function() {
  mongoose.connection.close(function () {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0);
    });
});

module.exports =  {
    mongoose,
    model : {
        User : mongoose.model( 'User', userSchema )
    }
};
