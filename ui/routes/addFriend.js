'use strict';

const request         = require( 'superagent' );
const _               = require( 'lodash' );
const  authMiddleware = require( '../middleware/authUser' );

module.exports =
    {
    method : 'GET',
    uri: '/addfriend/:email',
    middleware: [ authMiddleware ],
    handler: function( req, res ){
        request
        .post( 'http://localhost:5555/friends/add' )
        .send( {
            userEmail   :  _.get(req, 'session.user.email', 0 ),
            friendEmail : req.params.email
        } )
        .end( ( err, result ) => {
            if( err ){
                res.redirect( '/findfriends?error=true' );
            }
            else{
                res.redirect( '/findfriends?success=true' );
            }
        } );
    }
};
