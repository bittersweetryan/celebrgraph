'use strict';

var authMiddleware = require( '../middleware/authUser' );
var request        = require( 'superagent' );
var _              = require( 'lodash' );

module.exports = [
    {
        method : 'POST',
        uri: '/search',
        middleware: [ authMiddleware ],
        handler: function( req, res ){
            const session = req.session;
            const postBody = {
                searchTerm : _.get( req, 'body.searchTerm', '' ),
                email  : _.get( session, 'user.email', 0 )
            };

            request
                .post( 'http://localhost:5555/search' )
                .send( postBody )
                .end( function( err, result ){
                    const data = _.assign( req.data, result.body );
                    res.render( 'search', data );
                } );
        }
    },
    {
        method : 'GET',
        uri: '/search',
        handler: function( req, res ){
            res.redirect( '/' );
        }
    }
]
