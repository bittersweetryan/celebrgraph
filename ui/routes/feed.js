'use strict';

const authMiddleware = require( '../middleware/authUser' );
const request        = require( 'superagent' );
const _              = require( 'lodash' );

module.exports = {
    method : 'GET',
    uri: '/feed',
    middleware: [ authMiddleware ],
    handler: function( req, res ){
        const session = req.session;

        request
            .post( 'http://localhost:5555/feed' )
            .send( { email : session.user.email } )
            .end( function( err, result ){
                var data = _.assign( req.data, result.body );
                res.render( 'feed', data );
            } );
    }
};
