'use strict';

var authMiddleware = require( '../middleware/authUser' );
var request        = require( 'superagent' );
var _              = require( 'lodash' );

module.exports = {
    method : 'GET',
    uri: '/findfriends',
    middleware: [ authMiddleware ],
    handler: function( req, res ){

        const success = _.chain( req ).get( 'query.success', '' ).escape().value();
        const error   = _.chain( req ).get( 'query.error', '' ).escape().value();
        const session = req.session;

        request
            .post( 'http://localhost:5555/friends/list' )
            .send( { email : _.get( session, 'user.email', 0 ) } )
            .end( function( err, result ){
                var data = _.assign( req.data, { users: _.get( result, 'body.users', [] ), success, error } );

                res.render( 'findfriends', data );
            } );
    }
};
