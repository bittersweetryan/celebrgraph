'use strict';

var fs = require( 'fs' );
var _  = require( 'lodash' );

function installRoute( app, route ){
    console.log( `installing ${route.method} route for ${route.uri}...` );

    var handler;

    if( _.isArray( route.middleware ) ){
        handler = route.middleware;
        handler.push( route.handler );
    }
    else{
        handler = route.handler;
    }

    app[ route.method.toLowerCase() || 'get' ]( route.uri, handler );
}

function routes( app, done ){
    fs.readdir( './routes', ( err, files ) =>{
        if( !err ){
            files.filter( ( file ) => {
                return file !== 'index.js';
            } ).forEach( ( routedef ) => {
                var route = require( `./${routedef}` );

                if( _.isArray( route ) ){
                    route.forEach( function ( routeDef ){
                        installRoute( app, routeDef );
                    } );
                }
                else{
                    installRoute( app, route );
                }
            } );

            done();
        }
    } );
};

module.exports = routes;
