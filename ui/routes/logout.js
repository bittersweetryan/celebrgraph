'use strict';

const request = require( 'superagent' );
const _       = require( 'lodash' );

module.exports = [
    {
        method : 'GET',
        uri: '/logout',
        handler: function( req, res ){
            req.session.loggedIn = false;
            delete req.session.user;
            res.redirect( '/' );
        }
    }
];
