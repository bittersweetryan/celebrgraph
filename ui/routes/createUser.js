'use strict';

const request = require( 'superagent' );
const _       = require( 'lodash' );

module.exports = [
    {
        method : 'POST',
        uri: '/user/add',
        handler: function( req, res ){
            request
                .post( 'http://localhost:5555/user/add' )
                .send( req.body )
                .end( ( err, result ) => {
                    const session = req.session;

                    if( err ){
                        res.redirect( '/user/add' );
                    }
                    else{
                        session.loggedIn = true;
                        session.user = _.get( result, 'body.user', {} );
                        res.redirect( '/feed' );
                    }
                } );
        }
    },
    {
        method : 'GET',
        uri: '/user/add',
        handler: function( req, res ){
            res.render( 'createUser' );
        }
    }
];
