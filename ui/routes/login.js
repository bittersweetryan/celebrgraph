'use strict';

const request = require( 'superagent' );
const _       = require( 'lodash' );

module.exports = [
    {
        method : 'POST',
        uri: '/login',
        handler: function( req, res ){

            request
                .post( 'http://localhost:5555/login' )
                .send( req.body )
                .end( function( err, result ){
                    const session = req.session;
                    if( err || !_.get( result, 'body.success', false )){
                        res.redirect( '/?loginError=true' );
                    }
                    else{
                        session.loggedIn = true;
                        session.user = _.get( result, 'body.user', {} )
                        res.redirect( '/feed' );
                    }
                } );
        }
    },
    {
        method : 'GET',
        uri: '/login',
        handler: function( req, res ){
            res.redirect( '/' );
        }
    }
];
