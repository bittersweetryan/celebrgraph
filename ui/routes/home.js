'use strict';

module.exports = {
    method : 'GET',
    uri: '/',
    handler: function( req, res ){
        const error = req.query.loginError;

        if( req.session && req.session.user ){
            return res.redirect( 'feed' );
        }

        if( error ){
            req.data.loginError = true;
        }

        res.render( 'home', req.data );
    }
};
