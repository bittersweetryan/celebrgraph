'use strict'
const express      = require( 'express' );
const bodyParser   = require( 'body-parser' );
const cookieParser = require( 'cookie-parser' );
const handlebars   = require( 'express-handlebars' );
const session      = require( 'express-session' );
const routes       = require( './routes' );
const config       = require( './config/config' );
const url          = require( 'url' );
const superagent   = require( 'superagent' );
const commonData   = require( './middleware/data' );
const _            = require( 'lodash' );

const app = express();

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true  }) );

app.use( cookieParser() );
app.use( express.static( 'public' ) );

app.use( session( {
    secret: 'mmmmm unexplained bacon',
    saveUninitialized: false,
    resave : false
} ) );

app.use( commonData );

const handlebarsConfig = {
    defaultLayout : 'main',
    extname: '.hbs'
}

if( config( 'standAlone' ) ){
    console.warn( '*************************************' );
    console.warn( '*************************************' );
    console.log( 'USING STAND ALONE MODE' );
    console.warn( '*************************************' );
    console.warn( '*************************************' );
    const superagentReturn = function( urlPath ){
        return {
            send : function(){
                return{
                    end : function(cb){
                        const fullPath = url.parse( urlPath ).path;
                        const path = fullPath.substring( 1 );
                        const fileName = _.replace( path, '/', '_' );
                        let ret;

                        try{
                            ret = require( './standaloneData/' + fileName + '.json' );
                        }
                        catch( ex ){
                            ret = {};
                        }

                        cb( null, { body : ret } );
                    }
                }
            }
        }
    }

    superagent.post = superagentReturn;
    superagent.get  = superagentReturn;
}

app.engine( '.hbs', handlebars( handlebarsConfig ) );
app.set( 'view engine', '.hbs' );

routes( app, function done(){
    app.listen( 8080, function(){
        console.log( 'Celebrgraph UI listening on port 8080' );
    } )
} );

