# Getting Started

1) clone this repository

     `git clone git@github.com:bittersweetryan/celbrgraph.git`

2) Make sure you have node.js installed, if you don't grab an install and install it from http://nodejs.org

3) open the celebrgraph directory then `npm install`

    this will install all the projects depenndencies

4) Run `node server.js`

5) Use your web browser to navigate to http://localhost:8080

6) To run WITHOUT the api server open /config/config.json and set `standAlone` equal to `true`
