var config = require( './config.json' );

module.exports = function( key ){
    if( config.hasOwnProperty( key ) ){
        return config[ key ];
    }

    return false;
}
