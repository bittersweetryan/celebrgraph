'use strict';

const _ = require( 'lodash' );

function data( req, res, next ){
    const user = _.get( req, 'session.user', {} );
    const serverDefs = {
        serverDefs : {
            baseURL : `http://${req.hostname}:8080`
        }
    };

    const data =  _
        .chain( req )
        .get( 'data', {} )
        .assign( serverDefs, { user } )
        .value();

    data.loggedIn = req.session.loggedIn;
    req.data = data;

    next();
}

module.exports = data;
