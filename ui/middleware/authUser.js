'use strict';

var _ = require( 'lodash' );

function authUser( req, res, next ){
    if( !_.get( req, 'session.loggedIn', false )){
        res.redirect( '/' );
    }
    else{
        next();
    }
}

module.exports = authUser;
